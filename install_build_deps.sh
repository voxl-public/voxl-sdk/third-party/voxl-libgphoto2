#!/bin/bash
################################################################################
# Copyright (c) 2022 ModalAI, Inc. All rights reserved.
################################################################################

if [[ ! -f libgphoto2-2.5.26.tar.gz ]]; then
    wget https://sourceforge.net/projects/gphoto/files/libgphoto/2.5.26/libgphoto2-2.5.26.tar.gz
fi
if [[ ! -d libgphoto2-2.5.26 ]]; then
    tar -xzvf libgphoto2-2.5.26.tar.gz
    patch -p1 -N < patch/configure.patch
fi
